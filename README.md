# Simple web crawler

This is a simple web crawler which reads the crawled website sitemap and visits each link from the same domain.
For each visited link it extracts the urls found on that HTML page.

**NOTE**: This won't work (for now) if there are nested sitemaps.

## Configuration

You can configure the following environment variables (`.env` file):

* `CRAWLED_URL`
    * **Type:** `String`
    * **Description**: Represents the base url for crawling.
    * **Default**: `https://example.com`
* `SITEMAP_FILE_NAME`
    * **Type:** `String`
    * **Description**: Represents the file name which contains the sitemap on the website.
    * **Default**: `sitemap.xml`
* `REQUEST_TIMEOUT`
    * **Type:** `Integer`
    * **Description**: Represents the timeout used for a web request (in seconds).
    * **Default**: `1` (seconds)
* `RETRY_COUNT`
    * **Type:** `Integer`
    * **Description**: Represents the number of maximum retries for a web request.
    * **Default**: `5`
* `RETRY_DELAY`
    * **Type:** `Integer`
    * **Description**: Represents the number of seconds to wait before a retry.
    * **Default**: `1`
* `OUTPUT_FILE`
    * **Type:** `String`
    * **Description**: Represents the output file name. If is `console` then `stdout` will be used as output.
    * **Default**: `console`
* `EXTRACT_ONLY_ABSOLUTE_URLS`
    * **Type:** `Boolean`
    * **Description**: If `true`, relative urls will be ignored when parsing a web page.
    * **Default**: `false`
* `VERBOSE`
    * **Type:** `Boolean`
    * **Description**: If `true`, log level will be set to `INFO`. Default log level will be `ERROR`.
    * **Default**: `false`

## How to use ?

1. Configure `.env` as you want
2. Run `poetry install` (Only the first time)
3. Run `poetry run python -m crawler.main`
