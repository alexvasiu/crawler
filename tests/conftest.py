import mock
import pytest
from functools import wraps
from urllib.parse import urljoin


def _fake_retry(*args, **kwargs):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            return f(*args, **kwargs)

        return wrapper

    return decorator


FAKE_RETRY = mock.Mock(_fake_retry, wraps=_fake_retry)


@pytest.fixture(scope="function")
def retry():
    with mock.patch("tenacity.retry", FAKE_RETRY) as retry:
        yield retry


@pytest.fixture()
def sitemap_parser(retry):
    from crawler.parsers.sitemap_parser import SitemapParser
    from crawler.config import CRAWLED_URL, SITEMAP_FILE_NAME

    return SitemapParser(urljoin(CRAWLED_URL, SITEMAP_FILE_NAME))


@pytest.fixture()
def page_parser(retry):
    from crawler.parsers.page_parser import PageParser
    from crawler.config import CRAWLED_URL

    return PageParser(CRAWLED_URL)


@pytest.fixture()
def web_crawler(retry):
    from crawler.web_crawler import WebCrawler
    from crawler.config import CRAWLED_URL, SITEMAP_FILE_NAME

    return WebCrawler(CRAWLED_URL, SITEMAP_FILE_NAME)
