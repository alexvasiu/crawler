PAGE_WITHOUT_LINKS = """
<!DOCTYPE html>
<html>
<body>
<h1>My First Heading</h1>
<p>My first paragraph.</p>
</body>
</html>
""".strip()

PAGE_WITH_LINKS = """
<!DOCTYPE html>
<html>
<body>
<h1>My First Heading</h1>
<p>My first paragraph.</p>
<a href="/">aaa</a>
<link href="/">aaa2</link>
<a href="https://google.com">aaa2</a>
<a>aa3</a>
</body>
</html>
""".strip()
