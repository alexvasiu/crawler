WRONG_SITEMAP = """
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
  <a>
    <loc>https://monzo.com/blog/celebrating-black-history-month-2020/</loc>
    <lastmod>2020-10-10</lastmod>
  </a>
</urlset>
""".strip()

VALID_SITEMAP1 = """
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
  <url>
    <loc>https://monzo.com/blog/celebrating-black-history-month-2020/</loc>
    <lastmod>2020-10-10</lastmod>
  </url>
</urlset>
""".strip()

VALID_SITEMAP2 = """
<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.google.com/schemas/sitemap/0.84">
  <sitemap>
    <loc>https://monzo.com/blog/celebrating-black-history-month-2020/</loc>
  </sitemap>
</sitemapindex>
""".strip()


VALID_SITEMAP3 = """
<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.google.com/schemas/sitemap/0.84">
  <sitemap>
    <loc>https://monzo.com/blog/celebrating-black-history-month-2020/</loc>
  </sitemap>
  <sitemap>
    <loc>https://monzo.com/search/sitemap.xml</loc>
  </sitemap>
</sitemapindex>
""".strip()


VALID_SITEMAP4 = """
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">
  <url>
    <loc>https://monzo.com/blog/celebrating-black-history-month-2020/</loc>
    <lastmod>2020-10-10</lastmod>
  </url>
  <url>
    <loc>https://monzo.com/blog/celebrating-black-history-month-2020/aa.pdf</loc>
    <lastmod>2020-10-10</lastmod>
  </url>
  <url>
    <loc>https://facebook.com</loc>
    <lastmod>2020-10-10</lastmod>
  </url>
</urlset>
""".strip()
