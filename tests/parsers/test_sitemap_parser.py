from urllib.parse import urljoin

import pytest
import requests

from crawler import config
from tests.data.sitemap_data import (
    WRONG_SITEMAP,
    VALID_SITEMAP1,
    VALID_SITEMAP2,
    VALID_SITEMAP3,
)


@pytest.mark.parametrize("sitemap", [VALID_SITEMAP1, VALID_SITEMAP2, VALID_SITEMAP3])
def test_valid_sitemaps(responses, sitemap_parser, sitemap):
    responses.add(
        responses.GET,
        urljoin(config.CRAWLED_URL, config.SITEMAP_FILE_NAME),
        json=sitemap,
    )

    result = sitemap_parser.parse()

    assert result == ["https://monzo.com/blog/celebrating-black-history-month-2020/"]


@pytest.mark.parametrize("sitemap", ["", WRONG_SITEMAP])
def test_wrong_sitemaps(responses, sitemap_parser, sitemap):
    responses.add(
        responses.GET,
        urljoin(config.CRAWLED_URL, config.SITEMAP_FILE_NAME),
        json=sitemap,
    )

    result = sitemap_parser.parse()

    assert result == []


def test_invalid_url(sitemap_parser):
    with pytest.raises(requests.RequestException):
        sitemap_parser.parse()
