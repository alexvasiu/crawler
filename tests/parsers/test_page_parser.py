import pytest
import requests

from crawler import config
from tests.data.page_data import PAGE_WITH_LINKS, PAGE_WITHOUT_LINKS


@pytest.mark.parametrize(
    "page,absolut_urls_only,urls",
    [
        (PAGE_WITHOUT_LINKS, False, set()),
        (PAGE_WITH_LINKS, False, {"/", "https://google.com"}),
        (PAGE_WITH_LINKS, True, {"https://google.com"}),
    ],
)
def test_page_parser(
    monkeypatch, responses, page_parser, page, absolut_urls_only, urls
):
    from crawler.parsers import page_parser as pp

    monkeypatch.setattr(pp, "EXTRACT_ONLY_ABSOLUTE_URLS", absolut_urls_only)

    responses.add(
        responses.GET,
        config.CRAWLED_URL,
        json=page,
    )

    result = page_parser.parse()

    assert result == urls


def test_invalid_url(page_parser):
    with pytest.raises(requests.RequestException):
        page_parser.parse()
