from urllib.parse import urljoin

import pytest

from crawler import config
from tests.data.page_data import PAGE_WITH_LINKS, PAGE_WITHOUT_LINKS
from tests.data.sitemap_data import VALID_SITEMAP4

PAGE_URL1 = "https://monzo.com/blog/celebrating-black-history-month-2020/"
PAGE_URL2 = "https://monzo.com/blog/celebrating-black-history-month-2020/aa.pdf"
PAGE_URL3 = "https://facebook.com"


@pytest.mark.parametrize(
    "page,urls",
    [(PAGE_WITHOUT_LINKS, set()), (PAGE_WITH_LINKS, {"/", "https://google.com"})],
)
def test_web_crawler_valid_data(responses, web_crawler, page, urls):
    responses.add(
        responses.GET,
        urljoin(config.CRAWLED_URL, config.SITEMAP_FILE_NAME),
        json=VALID_SITEMAP4,
    )

    responses.add(responses.GET, PAGE_URL1, json=page)

    web_crawler.crawl()

    assert web_crawler.results == {PAGE_URL1: urls, PAGE_URL2: set(), PAGE_URL3: set()}


def test_web_crawler_no_sitemap_data(web_crawler):
    web_crawler.crawl()

    assert web_crawler.results == {}


def test_web_crawler_no_page_data(responses, web_crawler):
    responses.add(
        responses.GET,
        urljoin(config.CRAWLED_URL, config.SITEMAP_FILE_NAME),
        json=VALID_SITEMAP4,
    )

    web_crawler.crawl()

    assert web_crawler.results == {PAGE_URL1: set(), PAGE_URL2: set(), PAGE_URL3: set()}
