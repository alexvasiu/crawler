"""Requests mixin file."""
import logging

import requests
from tenacity import retry, stop_after_attempt, wait_fixed, retry_if_exception_type

from crawler.config import REQUEST_TIMEOUT, RETRY_COUNT, RETRY_DELAY

logger = logging.getLogger()


class RequestsMixin:
    """Requests mixin class."""

    @retry(
        stop=stop_after_attempt(RETRY_COUNT),
        wait=wait_fixed(RETRY_DELAY),
        reraise=True,
        retry=retry_if_exception_type(requests.Timeout),
    )
    def retrieve_page(self, url: str) -> str:
        """Retrieve a web page.

        :param url: The url of the web page.
        :return: The data text on the page.
        """
        logger.info("Try to retrieve a web page", extra={"url": url})
        try:
            request_response = requests.request(
                "GET", url, allow_redirects=False, timeout=REQUEST_TIMEOUT
            )
            request_response.raise_for_status()
            return request_response.text
        except requests.exceptions.RequestException as error:
            logger.error(
                "Failed to retrieve a web page", extra={"url": url, "error": str(error)}
            )
            raise
