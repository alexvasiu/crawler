"""Parser file."""
from abc import ABC, abstractmethod

from bs4 import BeautifulSoup
from bs4.element import SoupStrainer

from crawler.utils.requests_mixin import RequestsMixin


class Parser(ABC, RequestsMixin):
    """Parser class."""

    def __init__(self, base_url: str, strainer: SoupStrainer = None):
        """Parser init.

        :param base_url: The base url for parsing.
        :param strainer: Strainer object used with BeautifulSoup.
        """
        super(RequestsMixin, self).__init__()
        self._base_url = base_url
        self._strainer = strainer

    def parse(self):
        """Retrieve and parse the web page.

        :return: The parsed data.
        """
        data = self.retrieve_page(self._base_url)
        soup = BeautifulSoup(data, "lxml", parse_only=self._strainer)

        return self._parse_data(soup, data)

    @abstractmethod
    def _parse_data(self, soup_object: BeautifulSoup, data: str):
        pass
