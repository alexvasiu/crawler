"""Page parser file."""
from typing import Set
from urllib.parse import urlparse

from bs4 import BeautifulSoup
from bs4.element import SoupStrainer

from crawler.config import EXTRACT_ONLY_ABSOLUTE_URLS
from crawler.parsers.parser import Parser


class PageParser(Parser):
    """Page parser class."""

    __links_tags = ["a", "link"]

    def __init__(self, page_url: str):
        """Page parser init.

        :param page_url: The page url.
        """
        super().__init__(page_url, SoupStrainer(self.__links_tags))

    @staticmethod
    def _filter_url(url: str) -> bool:
        if url == "":
            return False

        if EXTRACT_ONLY_ABSOLUTE_URLS and not urlparse(url).netloc:
            return False

        return True

    def _parse_data(self, soup_object: BeautifulSoup, data: str) -> Set[str]:
        translation = str.maketrans("", "", "\\'\"")
        return set(
            filter(
                self._filter_url,
                map(
                    lambda link: link.get("href", "").translate(translation),
                    soup_object.findAll(self.__links_tags),
                ),
            )
        )
