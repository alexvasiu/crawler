"""Sitemap parser file."""
from typing import List, Optional

from bs4 import BeautifulSoup
from bs4.element import SoupStrainer, Tag

from crawler.parsers.parser import Parser


class SitemapParser(Parser):
    """Sitemap parser class."""

    __sitemap_tags = ["sitemap", "url"]

    def __init__(self, sitemap_url: str):
        """Sitemap parser init.

        :param sitemap_url: The sitemap url.
        """
        super().__init__(sitemap_url, SoupStrainer(self.__sitemap_tags))

    @staticmethod
    def _parse_sitemap(sitemap: Tag) -> Optional[str]:
        url = sitemap.findNext("loc").text

        if url.endswith(".xml"):
            return None  # TODO: recursive
        else:
            return url

    def _parse_data(self, soup_object: BeautifulSoup, data: str) -> List[str]:
        return list(
            filter(
                None,
                map(
                    lambda sitemap: self._parse_sitemap(sitemap),
                    soup_object.find_all(self.__sitemap_tags),
                ),
            )
        )
