"""Main file."""
import logging
import sys
from contextlib import contextmanager
from datetime import datetime
from typing import Dict, Set

from pythonjsonlogger import jsonlogger

from crawler.config import CRAWLED_URL, SITEMAP_FILE_NAME, OUTPUT_FILE, VERBOSE
from crawler.web_crawler import WebCrawler


class CustomJsonFormatter(jsonlogger.JsonFormatter):
    """Custom Json Formatter class."""

    def add_fields(self, log_record, record, message_dict):
        """Add fields method."""
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)
        if not log_record.get("timestamp"):
            now = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
            log_record["timestamp"] = now
        if log_record.get("level"):
            log_record["level"] = log_record["level"].upper()
        else:
            log_record["level"] = record.levelname


def configure_logging():
    """Configure logging."""
    logger = logging.getLogger()
    log_handler = logging.StreamHandler()
    formatter = CustomJsonFormatter("%(timestamp)s %(level)s %(name)s %(message)s")
    log_handler.setFormatter(formatter)
    logger.addHandler(log_handler)
    logger.setLevel(logging.INFO if VERBOSE else logging.ERROR)


@contextmanager
def get_output_file_handler():
    """Get the output file handler based on OUTPUT_FILE env variable."""
    if OUTPUT_FILE == "console":
        yield sys.stdout
    else:
        with open(OUTPUT_FILE, "w") as output_file:
            yield output_file


def display_results(results: Dict[str, Set[str]]) -> None:
    """Print the crawl results.

    :param results: The crawl results.
    """
    with get_output_file_handler() as output_file:
        for page_url, urls in results.items():
            output_file.write(f"Page {page_url} :\n")
            for url in sorted(urls):
                output_file.write(f"\t - {url}\n")


if __name__ == "__main__":
    configure_logging()
    crawler = WebCrawler(CRAWLED_URL, SITEMAP_FILE_NAME)
    crawler.crawl()
    display_results(crawler.results)
