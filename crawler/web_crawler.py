"""Web crawler file."""
import concurrent.futures as future
import logging
from os.path import splitext
from typing import Tuple, Dict
from urllib.parse import urljoin, urlparse

import requests

from crawler.parsers.page_parser import PageParser
from crawler.parsers.sitemap_parser import SitemapParser

logger = logging.getLogger()


class WebCrawler:
    """Web crawler class."""

    __allowed_file_extensions = ["", ".htm", ".html"]

    def __init__(self, base_url: str, sitemap_file_name: str):
        """Web crawler init.

        :param base_url: The url to crawl.
        :param sitemap_file_name: The sitemap file name.
        """
        self._base_url = base_url
        self._sitemap_file_name = sitemap_file_name
        self._domain = urlparse(base_url).hostname
        self._result = {}

    def _validate_url(self, url: str) -> Tuple[bool, str]:
        parsed_url = urlparse(url)

        if parsed_url.hostname != self._domain:
            return False, "The url domain is not the same for the base url"

        _, file_extension = splitext(parsed_url.path)
        if file_extension not in self.__allowed_file_extensions:
            return False, "The url file doesn't have a valid extension"

        return True, ""

    def _extract_urls(self, page_url: str) -> None:
        validation_result, validation_error = self._validate_url(page_url)
        page_result = set()
        if validation_result:
            try:
                page_parser = PageParser(page_url)
                page_result = page_parser.parse()
            except requests.exceptions.RequestException as exception:
                logger.warning(
                    "Failed to obtain web page",
                    extra={"url": page_url, "error": str(exception)},
                )
        else:
            logger.warning(
                "Invalid page url",
                extra={"url": page_url, "error": validation_error},
            )

        self._result[page_url] = page_result

    def crawl(self) -> None:
        """Crawl method."""
        self._result = {}
        sitemap_url = urljoin(self._base_url, self._sitemap_file_name)
        try:
            sitemap_parser = SitemapParser(sitemap_url)
            urls = sitemap_parser.parse()
        except requests.exceptions.RequestException as exception:
            logger.error(
                "Failed to obtain sitemap",
                extra={"url": sitemap_url, "error": str(exception)},
            )
            return

        with future.ThreadPoolExecutor() as workers:
            workers.map(self._extract_urls, urls)

    @property
    def results(self) -> Dict:
        """The results of the crawling process."""
        return self._result
