"""Config file."""
import os

from dotenv import load_dotenv
from distutils.util import strtobool

load_dotenv()

CRAWLED_URL = os.getenv("CRAWLED_URL", "https://example.com")
SITEMAP_FILE_NAME = os.getenv("SITEMAP_FILE_NAME", "sitemap.xml")
REQUEST_TIMEOUT = int(os.getenv("REQUEST_TIMEOUT", "1"))
RETRY_COUNT = int(os.getenv("RETRY_COUNT", "5"))
RETRY_DELAY = int(os.getenv("RETRY_DELAY", "1"))
OUTPUT_FILE = os.getenv("OUTPUT_FILE", "console")
EXTRACT_ONLY_ABSOLUTE_URLS = strtobool(os.getenv("EXTRACT_ONLY_ABSOLUTE_URLS", "false"))
VERBOSE = strtobool(os.getenv("VERBOSE", "false"))
